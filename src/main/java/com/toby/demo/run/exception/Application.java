package com.toby.demo.run.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	/**
	 * 欢迎关注微信公众号【肥朝】
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
}
